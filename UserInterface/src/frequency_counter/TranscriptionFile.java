package frequency_counter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;


public class TranscriptionFile {

	XWPFDocument docx ;
	XWPFWordExtractor wordExtractor ;
	
	String documentText ;
	HashMap<String,String> persons ;
	String[] header ;
	
	public TranscriptionFile( String filepath )
	{
		try {
			this.docx = new XWPFDocument( new FileInputStream(filepath) ) ;
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.wordExtractor = new XWPFWordExtractor( this.docx ) ;
		this.documentText = this.wordExtractor.getText() ;
		
		this.persons = new HashMap<String,String>();
		this.extractPersons();
		this.extractInfo() ;
		this.refractorDocxText() ;
	}
	
	
	public HashMap<String,String> getActors()
	{
		return this.persons ;
	}
	
	
	public String getDocument()
	{
		return this.documentText ;
	}
	
	/**
	 * Returns a String with all the words spoken by 'actorChar'. If 'actorChar' doesn't exist, this function returns null
	 * @param actorChar
	 * @return
	 */
	public String filter( String actorChar )
	{
		if ( this.persons.containsKey( actorChar ) )
		{
			String[] temp = this.documentText.split("(\n|\r|\r\n)") ;
			
			String result = new String() ;
			
			for ( int i=0 ; i<temp.length ; i++ )
			{
				temp[i] = temp[i].trim() ;
				
				if ( temp[i].startsWith(actorChar) )
				{
					if ( temp[i].length() > 2 )
					{
						result += temp[i].substring(2, temp[i].length()-1).trim() + " " ;
					}
				}
			}
			
			return result.trim() ;

		}
		
		return null;
	}
	
	/**
	 * Extracts all the names and symbols used on the document to identify the ones involved
	 */
	private void extractPersons()
	{
		String temp = this.documentText.substring( this.documentText.indexOf("\n")+1 ) ;
		
		temp = temp.substring(1,temp.indexOf("\n")-1) ;
		
		String[] people = temp.split(",") ;
		
		for ( String s : people )
		{
			String[] s_temp = s.split(":") ;			
			this.persons.put(s_temp[0].trim(), s_temp[1].trim() );
		}
	}
	
	/**
	 * Extracts the actors
	 */
	private void extractInfo()
	{
		String temp = this.documentText.substring(1, this.documentText.indexOf("\n")-1 ) ;
				
		this.header = temp.split(",");
	}
	
	/**
	 * Removes the header of the document
	 */
	private void refractorDocxText()
	{
		this.documentText = this.documentText.substring( this.documentText.indexOf("\n")+1 ) ;
		this.documentText = this.documentText.substring( this.documentText.indexOf("\n")+1 ) ;
		
		this.documentText = this.documentText.replace("(", "") ;
		this.documentText = this.documentText.replace(")", "") ;
		this.documentText = this.documentText.replace(",", "") ;
		this.documentText = this.documentText.replace("?", "") ;
		this.documentText = this.documentText.replace(".", "") ;
		this.documentText = this.documentText.replace("!", "") ;
		this.documentText = this.documentText.replace("\\", "") ;
		this.documentText = this.documentText.replace("/", "") ;
		this.documentText = this.documentText.replace(":", "") ;
	}
	
	
}