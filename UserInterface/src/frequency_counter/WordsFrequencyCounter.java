package frequency_counter;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Arrays;


public class WordsFrequencyCounter{

	
	public static HashMap<String, HashMap<String,Frequency> > countFulltext( TranscriptionFile file , WordsCollection dictionary, boolean distinctActors) 
	{					
		String[] words = file.getDocument().split("(\\s+)") ;
		
		if ( ! distinctActors )
		{
			for ( String word : words ) 
			{			
				if ( dictionary.contains( word ) )
				{
					dictionary.incFrequency( word ) ;
				}
			}
		}
		else
		{
			HashMap<String,String> actors = file.getActors() ;
			String speaker = "" ;
			
			for ( String word : words )
			{
				boolean actorTag = false ;
				
				for ( Entry<String,String> actor : actors.entrySet() )
					if ( word.equals( actor.getKey() ) )
					{
						speaker = actor.getKey() ;
						actorTag = true ;
					}
				
				if ( ! actorTag )
					if ( dictionary.contains( word ) )
						dictionary.incFrequency( word , speaker ) ;
			}
		}

		return dictionary.getResultsWithActors(true) ;
	}
	
	public static HashMap<String,Frequency> countFulltextByActor( TranscriptionFile file , String actorChar , WordsCollection dictionary )
	{	
		countFulltext(file,dictionary,true) ;
		HashMap<String, HashMap<String,Frequency> > temp =  dictionary.getResultsWithActors(true) ;
		
		WordsCollection tempWords = new WordsCollection() ;
		
		for ( Entry <String, HashMap<String,Frequency>> entry : temp.entrySet() )
			for ( Entry <String,Frequency> entry2 : entry.getValue().entrySet() )
			{
				if ( entry2.getKey().equals(actorChar) )
				{
					tempWords.addNewEntry( entry.getKey() , entry2.getValue().get() );
					break;
				}
			}
		
		return tempWords.getResults(false) ;
	}
	
	public static HashMap<Integer, WordsCollection> countFulltextBlocksByActor( TranscriptionFile file , WordsCollection dictionary , String actorChar , int blockSize, int blockStep )
	{
		HashMap<Integer, WordsCollection> temp = WordsFrequencyCounter.countFulltextBlocks( file , dictionary , true , blockSize , blockStep );
		
		HashMap<Integer, WordsCollection> aux = new HashMap<Integer, WordsCollection>();
		WordsCollection auxW = new WordsCollection() ;
		
		for( Entry<Integer, WordsCollection> entry : temp.entrySet() ) // for each block...
		{	
			for( String key : entry.getValue().getResultsWithActors(false).keySet() ) // for each word..
			{
				HashMap<String, HashMap<String,Frequency> > hashTemp = entry.getValue().getResultsWithActors(true) ;
				for ( String actor : hashTemp.get(key).keySet() ) // for each actor...
				{
					if ( actor.equals(actorChar) )
					{
						auxW.addNewEntry( key , hashTemp.get(key).get(actor).get() );
						aux.put( entry.getKey() , auxW ) ;
						break;
					}
					
				}
			}
			auxW = new WordsCollection() ;
		}
		
		return aux ;
	}

	public static HashMap<Integer, WordsCollection> countFulltextBlocks( TranscriptionFile file , WordsCollection dictionary, boolean distinctActors , int blockSize, int blockStep )
	{
		String[] words = file.getDocument().split("(\\s+)") ;
				
		if ( ! distinctActors )
		{
			return countFulltextBlocksAux( file , dictionary , words , blockSize , blockStep ) ;
		}
		else
		{
			return countFulltextBlocksAuxActors( file , dictionary , words , blockSize , blockStep ) ;
		}
	}

	private static HashMap<Integer, WordsCollection> countFulltextBlocksAuxActors( TranscriptionFile file , WordsCollection dictionary , String[] words , int blockSize , int blockStep )
	{
		HashMap<Integer, WordsCollection> result = new HashMap<Integer, WordsCollection>() ;
		Integer blockCounter = new Integer(1) ;
		HashMap<String,String> actors = file.getActors() ;
		String speaker = "" ;
		
		for ( int i=0 ; i<(words.length-blockSize+blockStep) ; i=i+blockStep )
		{
			String [] tempWords ; // the block to be processed
			
			
			if ( i+blockSize <= words.length )
				tempWords = Arrays.copyOfRange( words , i , (i+blockSize) ) ; // full size
			else
				tempWords = Arrays.copyOfRange( words , i , words.length ) ; // last block with the words left
			
			
			WordsCollection wc_temp = new WordsCollection( actors ) ;
			
			for ( String word : tempWords )
			{
				boolean actorTag = false ;
				
				for ( Entry<String,String> actor : actors.entrySet() )
				{
					if ( word.equals( actor.getKey() ) )
					{
						speaker = actor.getKey() ;
						actorTag = true ;
					}
				}
				
				if ( !actorTag )
					if ( dictionary.contains( word ) )
					{
						if ( wc_temp.contains( word ) )
						{
							wc_temp.incFrequency( word , speaker ) ;
						}
						else
						{
							wc_temp.addNewEntry( word ) ;
							wc_temp.incFrequency( word , speaker ) ;
						}
					}
			}
			
			result.put( blockCounter , wc_temp ) ;
			blockCounter++ ;
		}
		
		return result ;
	}
	
	private static HashMap<Integer, WordsCollection> countFulltextBlocksAux( TranscriptionFile file , WordsCollection dictionary , String[] words , int blockSize , int blockStep )
	{
		HashMap<Integer, WordsCollection> result = new HashMap<Integer, WordsCollection>() ;
		Integer blockCounter = new Integer(1) ;
		
		for ( int i=0 ; i<(words.length-blockSize+blockStep) ; i=i+blockStep )
		{
			String [] tempWords ; // the block to be processed
			
			
			if ( i+blockSize <= words.length )
				tempWords = Arrays.copyOfRange( words , i , (i+blockSize) ) ; // full size
			else
				tempWords = Arrays.copyOfRange( words , i , words.length ) ; // last block with the words left
			
			
			WordsCollection wc_temp = new WordsCollection() ;
			
			for ( String word : tempWords )
			{
				if ( dictionary.contains( word ) )
				{
					if ( wc_temp.contains( word ) )
					{
						wc_temp.incFrequency( word ) ;
					}
					else
					{
						wc_temp.addNewEntry( word ) ;
						wc_temp.incFrequency( word ) ;
					}
				}
			}
			
			result.put( blockCounter , wc_temp ) ;
			blockCounter++ ;
		}
		
		return result ;
	}
	
}