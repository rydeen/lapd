package frequency_counter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class WordsCollection {

	private HashMap<String,Frequency> words ;
	private HashMap<String, HashMap<String,Frequency> > wordsByActors ;
	
	private HashMap<String,String> actors ;
	
	boolean usingActors ;
	
	public WordsCollection( )
	{
		this.usingActors = false ;
		this.words = new HashMap<String,Frequency>() ;
	}
	
	public WordsCollection( HashMap<String,String> actors )
	{
		this.usingActors = true ;
		this.actors = actors ;
		this.wordsByActors = new HashMap<String, HashMap<String,Frequency> > () ;
	}
	
	public WordsCollection( String filepath , HashMap<String,String> actors )
	{
		this.usingActors = true ;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder builder = null ;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		Document doc = null ;
		try {
			doc = builder.parse( new File(filepath+".xml") ) ;
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression getNode = null ;
		try {
			getNode = xpath.compile("//palavra");
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		NodeList nodes = null ;
		try {
			nodes = (NodeList) getNode.evaluate( doc , XPathConstants.NODESET );
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		this.wordsByActors = new HashMap<String, HashMap<String,Frequency> > () ;
		
		for ( int i=0 ; i<nodes.getLength() ; i++ )
		{
			if ( this.wordsByActors.containsKey(nodes.item(i)))
				System.out.println("REPETIDO!");
			
			HashMap<String,Frequency> auxF = new HashMap<String,Frequency>() ;
			for ( String key : actors.keySet() )
				auxF.put( key , new Frequency() ) ;
			
			this.wordsByActors.put( nodes.item(i).getTextContent(), auxF ) ;
		}
	}
	
	public WordsCollection( String filepath )
	{
		this.usingActors = false ;
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder builder = null ;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		Document doc = null ;
		try {
			doc = builder.parse( new File(filepath+".xml") ) ;
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression getNode = null ;
		try {
			getNode = xpath.compile("//palavra");
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		NodeList nodes = null ;
		try {
			nodes = (NodeList) getNode.evaluate( doc , XPathConstants.NODESET );
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		this.words = new HashMap<String,Frequency>() ;
		
		for ( int i=0 ; i<nodes.getLength() ; i++ )
		{
			if ( this.words.containsKey(nodes.item(i)))
				System.out.println("REPETIDO!");
				
			this.words.put( nodes.item(i).getTextContent(), new Frequency() ) ;
		}
	}
	
	public boolean contains( Object key )
	{
		if (! this.usingActors )
			return this.words.containsKey(key) ;
		else
			return this.wordsByActors.containsKey(key) ;
	}
	
	public boolean incFrequency( Object key )
	{
		if ( this.contains(key) )
		{
			if ( ! this.usingActors )
				this.words.get(key).inc();
			else
				return false ;
			
			return true ;
		}
		
		return false ;
	}
	
	public boolean incFrequency( Object key , String actorKey )
	{
		if ( this.contains(key) )
		{
			if ( this.usingActors )
				this.wordsByActors.get(key).get( actorKey ).inc() ;
			else
				return false ;
			
			return true ;
		}
		
		return false ;
	}
	
	public HashMap<String,Frequency> getResults( boolean filter )
	{
		if ( ! this.usingActors && filter )
		{
			HashMap<String,Frequency> aux = new HashMap<String,Frequency>() ;
			
			for( Entry<String,Frequency> entry : this.words.entrySet() )
			{
				if ( entry.getValue().get() != 0 )
					aux.put( entry.getKey(), entry.getValue() ) ;
			}
			
			return aux ;
		}
		else if ( ! this.usingActors )
			return this.words ;
		
		return null ;
	}
	
	public HashMap<String, HashMap<String,Frequency> > getResultsWithActors( boolean filter )
	{
		if ( this.usingActors && filter )
		{
			HashMap<String, HashMap<String,Frequency> > aux = new HashMap<String, HashMap<String,Frequency> >() ;
			
			for( Entry<String,HashMap<String,Frequency>> entry : this.wordsByActors.entrySet() )
			{
				boolean delete = true ;
				
				for( Entry<String,Frequency> entryFreq: entry.getValue().entrySet() )
				{
					if ( entryFreq.getValue().get() != 0 )
					{
						delete=false ;
						break;
					}
				}
				
				if ( ! delete )
					aux.put( entry.getKey(), entry.getValue() ) ;
			}
			
			return aux ;
		}
		else if ( this.usingActors )
		{
			return this.wordsByActors ;
		}
		
		return null ;
	}
	
	public void addNewEntry( String key )
	{
		if ( ! this.usingActors )
		{
			this.words.put(key, new Frequency() ) ;
		}
		else
		{
			HashMap<String,Frequency> temp = new HashMap<String,Frequency>() ;
			for ( String actorKey : this.actors.keySet() )
				temp.put( actorKey , new Frequency() ) ;
			
			this.wordsByActors.put(key, temp) ;
		}
		
	}
	
	public void addNewEntry( String key , int freq )
	{
		if ( ! this.usingActors )
		{
			this.words.put(key, new Frequency(freq) ) ;
		}
		else
		{
			HashMap<String,Frequency> temp = new HashMap<String,Frequency>() ;
			for ( String actorKey : this.actors.keySet() )
				temp.put( actorKey , new Frequency() ) ;
			
			this.wordsByActors.put(key, temp) ;
		}
	}
}
