package frequency_counter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map.Entry;

public class Main {

	
	public static void main( String[] args)
	{
		File file = new File("output.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		
		
		// TRANSCRIPTION FILE
		TranscriptionFile tf = new TranscriptionFile("P010_S1.docx") ;
		String s = "" ;

		s +="// ******************************************************************************** //\n";
		s +="//                                 DISTINCT ACTORS                                  //\n";
		s +="// ******************************************************************************** //\n";
		
		s +="// -------------------------- FULL TEXT ALL ACTORS -------------------------- // (1)\n";
		s += case1(tf) ;
		
		s +="// -------------------------- FULL TEXT  BY ACTOR T -------------------------- // (2)\n";
		s += case2(tf) ;
		
		s +="// -------------------------- BY BLOCKS ALL ACTORS -------------------------- // (3)\n";
		s += case3(tf) ;
		
		// TODO REVER ESTE METODO
		s +="// -------------------------- BY BLOCKS  BY ACTOR T-------------------------- // (4)\n";
		s += case4(tf) ;
		
		s +="// ******************************************************************************** //\n";
		s +="//                                 ALL ACTORS  THE SAME                             //\n";
		s +="// ******************************************************************************** //\n";


		s +="// -------------------------- FULL TEXT -------------------------- // (5)\n";
		s += case5(tf) ;

		s +="// -------------------------- BY BLOCKS -------------------------- // (6)\n";
		s += case6(tf) ;
		
		
		System.out.println(s);
	}
	
	
	
	
	/* ********************************************************************************************************* */
	/*											 DISTINCT ACTORS  									 			 */
	/* ********************************************************************************************************* */
	// -------------------------- FULL TEXT -------------------------- //
	public static String case1( TranscriptionFile tf )
	{
		WordsCollection wc_actors = new WordsCollection("exemplo", tf.getActors() ) ;
		
		// all actors (1)
		HashMap<String, HashMap<String,Frequency> > results1 = WordsFrequencyCounter.countFulltext(tf,wc_actors,true) ;

		String temp1 = "" ;
		for( String key : results1.keySet() ) 
		{
			temp1 += key + ": " ;
			
			for ( String actor : results1.get(key).keySet() )
			{
				temp1 += "\t" + actor + "="  + results1.get(key).get(actor).get() + "   ";
			}
			
			temp1+="\n";
		}
		
		return temp1;
	}
	public static String case2( TranscriptionFile tf )
	{
		WordsCollection wc_actors = new WordsCollection("exemplo", tf.getActors() ) ;
		
		// only for actor "T" (2)
		HashMap<String, Frequency> results2 =  WordsFrequencyCounter.countFulltextByActor(tf,"T",wc_actors) ;
		
		String temp2 = "" ;
		for( String key : results2.keySet() ) 
		{
			temp2+=key + ":  ";
			temp2+=results2.get(key).get() + "\n";
		}
		
		return temp2;
	}
	// -------------------------- BY BLOCKS -------------------------- //
	public static String case3( TranscriptionFile tf )
	{
		WordsCollection wc_actors = new WordsCollection("exemplo", tf.getActors() ) ;
		
		int blockStep1 = 10 ;
		int blockSize1 = 150 ;
		
		// all actors (3)
		HashMap<Integer, WordsCollection> results3 =
		WordsFrequencyCounter.countFulltextBlocks(tf,wc_actors,true,blockSize1,blockStep1);
		
		String temp3 = "" ;
		for( Entry<Integer, WordsCollection> entry : results3.entrySet() )
		{
			temp3+="> Block " + entry.getKey() +"\n";
			for( String key : entry.getValue().getResultsWithActors(false).keySet() ) 
			{
				temp3+=key + ":  " + "\n";
				
				for ( String actor : entry.getValue().getResultsWithActors(true).get(key).keySet() )
					temp3+= "\t" + actor + "="  + entry.getValue().getResultsWithActors(true).get(key).get(actor).get() + "\n";
			}
			temp3+="------------" +"\n";
		}
		
		return temp3;
	}
	public static String case4( TranscriptionFile tf )
	{	
		WordsCollection wc_noActors1 = new WordsCollection("exemplo") ;
		
		int blockStep1 = 10 ;
		int blockSize1 = 150 ;
		
		// only for actor X (4)
		
		HashMap<Integer, WordsCollection> results4 =
		WordsFrequencyCounter.countFulltextBlocksByActor(tf,wc_noActors1,"T",blockSize1,blockStep1);
		
		String temp4 = "" ;
		for( Entry<Integer, WordsCollection> entry : results4.entrySet() )
		{
			temp4+="> Block " + entry.getKey() +"\n";
			for( String key : entry.getValue().getResults(true).keySet() ) 
			{
				temp4+=key + ":  ";
				temp4 += entry.getValue().getResults(true).get(key).get() + "\n";
			}
			temp4+="------------" +"\n";
		}
		
		return temp4;
	}
	/* ********************************************************************************************************* */
	/*											 ALL ACTORS  THE SAME								 			 */
	/* ********************************************************************************************************* */
	// -------------------------- FULL TEXT -------------------------- // (5)	
	public static String case5( TranscriptionFile tf )
	{
		WordsCollection wc_noActors2 = new WordsCollection("exemplo") ;
		
		WordsFrequencyCounter.countFulltext(tf,wc_noActors2,false) ;
		
		HashMap<String, Frequency> results5 =  wc_noActors2.getResults(true) ;
		String temp5= "" ;
		for( String key : results5.keySet() ) 
		{
			temp5+=key + ":  ";
			temp5+=results5.get(key).get() +"\n";
		}
		
		return temp5;
	}
	// -------------------------- BY BLOCKS -------------------------- // (6)
	public static String case6( TranscriptionFile tf )
	{
		WordsCollection wc_noActors2 = new WordsCollection("exemplo") ;
		
		int blockStep2 = 10 ;
		int blockSize2 = 150 ;
		HashMap<Integer, WordsCollection> results6 =
		WordsFrequencyCounter.countFulltextBlocks(tf,wc_noActors2,false,blockSize2,blockStep2);
		
		String temp6 = "" ;
		for( Entry<Integer, WordsCollection> entry : results6.entrySet() )
		{
			temp6 +="> Block " + entry.getKey() + "\n";
			for( String key : entry.getValue().getResults(false).keySet() ) 
			{
				temp6 +=key + ": ";
				temp6 += entry.getValue().getResults(false).get(key).get() + "\n";
			}
			temp6 +="------------\n";
		}
		
		return temp6;
	}

	
}
