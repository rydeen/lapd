package frequency_counter;

public class Frequency {

	private int freq;
		
	public Frequency()
	{
		this.freq = 0 ;
	}
	
	public Frequency(int f)
	{
		this.freq = f ;
	}

	public int get()
	{
		return this.freq;
	}

	public void inc()
	{
		this.freq++;
	}
	
	public void set( int f )
	{
		this.freq = f;
	}
	
	public void reset()
	{
		this.freq = 0 ;
	}

}