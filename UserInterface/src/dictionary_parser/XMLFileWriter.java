package dictionary_parser;
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


public class XMLFileWriter {

	public static String XMLextension = ".xml" ;
	
	// xml elements
	public static String TAG_DICTIONARY = "dicionario" ;
	public static String TAG_CATEGORY = "categoria" ;
	public static String TAG_WORDS = "palavras" ;
	public static String TAG_WORD = "palavra" ;
	
	// xml attributes
	public static String ATTR_NAME = "nome" ;
	public static String ATTR_NUMBER = "numero" ;
	
	
	public static File CreateXMLFile( XMLCategory dictionary , String xml_filename )
	{
		
		try {
			
			// ------------------------------------- building document ------------------------------------- //
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument() ;
			
			// root element
			Element rootElement = doc.createElement( TAG_DICTIONARY ) ;
			
			// attributes of the root element
			Attr rootAttr = doc.createAttribute( ATTR_NAME ) ;
			rootAttr.setValue( dictionary.getCategoryName() ) ;
			rootElement.setAttributeNode( rootAttr ) ;
			
			// add root element
			doc.appendChild( rootElement ) ;
			
			// add child nodes of root
			for ( int i=0 ; i<dictionary.getNumCategories() ; i++ )
				recursiveCall( dictionary.getCategoryAt(i) , rootElement , doc ) ;
			
			
			// -------------------------------- write content into XML file -------------------------------- //
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance() ;
			Transformer transformer = transformerFactory.newTransformer() ;
			transformer.setOutputProperty("encoding", "ISO-8859-1") ;
			DOMSource source = new DOMSource( doc ) ;
			
			// set the output to be a new file
			StreamResult result = new StreamResult( new File( xml_filename ) ) ;
			
			// set the output to be the console
			// StreamResult result = new StreamResult( System.out ) ;
			
			// output to file
			transformer.transform( source ,  result );
			
			return new File(xml_filename) ;
			
		}
		catch ( Exception e ) 
		{
			//e.printStackTrace() ;
			return null ;
		}
		
	}
	
	public static void recursiveCall( XMLCategory category , Element element , Document doc )
	{
		if ( category.hasSubcategory() )
		{
			// category elements
			Element categoryElement = doc.createElement( TAG_CATEGORY ) ;
			// category attributes
			Attr categoryAttr = doc.createAttribute( ATTR_NAME ) ;
			categoryAttr.setValue( category.getCategoryName() ) ;
			categoryElement.setAttributeNode( categoryAttr ) ;
			
			// append category element
			element.appendChild( categoryElement ) ;
			
			for( int i=0 ; i<category.getNumCategories() ; i++ )
				recursiveCall( category.getCategoryAt(i) , categoryElement , doc ) ;
		}
		else
		{
			// last category element
			Element categoryElement = doc.createElement( TAG_CATEGORY ) ;
			// category attribute
			Attr categoryAttr = doc.createAttribute( ATTR_NAME ) ;
			categoryAttr.setValue( category.getCategoryName() ) ;
			categoryElement.setAttributeNode( categoryAttr ) ;
			
			// append category element
			element.appendChild( categoryElement ) ;
			
			// words element
			Element wordsElement = doc.createElement( TAG_WORDS ) ;
			// words attribute
			Attr wordsAttr = doc.createAttribute( ATTR_NUMBER ) ;
			wordsAttr.setValue( "" + category.getNumWords() );
			wordsElement.setAttributeNode( wordsAttr ) ;
			
			// append words element
			categoryElement.appendChild( wordsElement ) ;
			
			// add words
			Element wordElement ;
			Text textElement ;
			
			for ( int i=0 ; i<category.getNumWords() ; i++ )
			{
				wordElement = doc.createElement( TAG_WORD ) ;
				// word string value
				textElement = doc.createTextNode( category.getWordAt(i) ) ;
				wordElement.appendChild( textElement ) ;
				
				// append word
				wordsElement.appendChild( wordElement ) ;
			}
		}
	}
	
	
	/**
	 * Changes the name of the dictionary in 'filepath' to 'newname' and returns the file or null in case of error
	 * @param filepath
	 * @param newname
	 * @return
	 */
	public static File ChangeFilename( String filepath , String newname )
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		DocumentBuilder builder ;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return null ;
		}
		
		Document doc ;
		try {
			doc = builder.parse( new File(filepath+".xml") );
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			return null ;
		}

		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression getNode ;
		try {
			getNode = xpath.compile("/dicionario");
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null ;
		}
		
		NodeList nl;
		try {
			nl = (NodeList) getNode.evaluate( doc , XPathConstants.NODESET );
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null ;
		}
		
		nl.item(0).getAttributes().item(0).setTextContent( newname );
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance() ;
		
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
			return null ;
		}
		
		transformer.setOutputProperty("encoding", "ISO-8859-1") ;
		DOMSource source = new DOMSource( doc ) ;
		StreamResult result = new StreamResult( new File( newname+".xml" ) ) ;
		try {
			transformer.transform( source ,  result );
		} catch (TransformerException e) {
			e.printStackTrace();
			return null ;
		}
		
		(new File(filepath+".xml")).delete() ;
		
		return new File(newname+".xml") ;
	}
}
