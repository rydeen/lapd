package dictionary_parser;
import java.util.Vector;


public class XMLCategory {

	private String name ;
	
	private XMLCategory parent ;
	
	private Vector<String> words ;
	private Vector<XMLCategory> subcategories ;
	
	
	XMLCategory(String name) 
	{
		this.name = name ;
		this.parent = null ;
		this.words = new Vector<String>() ;
		this.subcategories = new Vector<XMLCategory>() ;
	}
	
	public void addWord( String word )
	{
		if ( ! words.contains( word ) && ! this.hasSubcategory() )
			words.add( word ) ;
	}
	
	public XMLCategory addCategory( String name ) 
	{
		if ( ! this.hasWords() )
		{
			XMLCategory result = this.getSubCategory(name) ;
			
			if ( result == null )
			{
				XMLCategory temp = new XMLCategory( name ) ;
				temp.setParent( this ) ;
				this.subcategories.add( temp );
				return temp ;
			}
			else
				return result ;
		}
		
		return null ;
	}
	
	public XMLCategory getSubCategory( String name )
	{
		for( int i=0 ; i<this.subcategories.size() ; i++ )
		{
			if ( this.subcategories.elementAt(i).getCategoryName().equals(name) )
				return this.subcategories.elementAt(i) ;
		}
		
		return null ;
	}
	
	public String getCategoryName() 
	{
		return this.name ;
	}
	
	public XMLCategory getRoot() 
	{
		if( this.parent == null )
			return this ;
		else
		{
			return this.parent.getRoot() ;
		}
	}
	
	public XMLCategory getParent()
	{
		return this.parent ;
	}

	public int getNumWords() 
	{
		if( this.hasWords() )
			return this.words.size() ;
		else
			return -1 ;
	}
	
	public int getNumCategories()
	{
		if ( this.hasSubcategory() )
			return this.subcategories.size() ;
		else
			return -1 ;
	}
	
	public String getWordAt( int index )
	{
		if( index<0 || index>this.words.size() )
			return "" ;
		else
			return this.words.elementAt( index ) ;
	}

	public XMLCategory getCategoryAt( int index)
	{
		if( index<0 || index>this.subcategories.size() )
			return null ;
		else
			return this.subcategories.elementAt( index ) ;
	}
	
	public void setParent( XMLCategory parent ) 
	{
		this.parent = parent ;
	}
	
	public void print() 
	{
		System.out.println( name ) ;
			
		if( this.words.size() == 0 )
		{
			for ( int i=0 ; i< this.subcategories.size() ; i++ )
				this.subcategories.elementAt(i).print();
		}
		else
		{
			for ( int i=0 ; i<this.words.size() ; i++ )
				System.out.println("** " + this.words.elementAt(i) ) ;
		}
	}

	public boolean hasWords()
	{
		if( this.words.size() == 0 )
			return false ;
		else
			return true ;
	}
	
	public boolean hasSubcategory() 
	{
		if( this.subcategories.size() == 0 )
			return false ;
		else
			return true ;
	}
}
