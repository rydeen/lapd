package dictionary_parser;
import java.io.File;


public class Main {

	public static void main( String [] args ) 
	{	
		// parse excel file
		String dictionaryName = "Dicionário" ;
		String excelDictionaryFileName = "BaseLexicalPortuguesa.xls" ;
		XMLCategory dictionary = Parser.ParseExcelDictionary( excelDictionaryFileName , dictionaryName ) ;
		
		// writing content to xml file
		String XMLFileName = "dicionario.xml" ;
		File xmlDictionary = XMLFileWriter.CreateXMLFile( dictionary , XMLFileName ) ;
		
		// validate xml file with xsd
		XMLFileValidator.ValidadeXMLFile( xmlDictionary ) ;
	}
	
}
