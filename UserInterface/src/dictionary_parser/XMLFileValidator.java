package dictionary_parser;
import java.io.File;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


public class XMLFileValidator {

	private static String XSDFileSchemaName = "db/dicionario.xsd" ;
	private static String NamePattern = "([a-zA-Z ãáàâéêíõôóúçÃÁÀÂÉÊÍÕÓÔÚÇ-])+" ;
	
	public static final Pattern XSDNamePattern = Pattern.compile(NamePattern) ;
	
	
	public static boolean ValidadeXMLFile( File xml_file )
	{
		File XSDFileSchema = new File( XSDFileSchemaName ) ;
		
		try 
		{
			SchemaFactory schemaFactory = SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI ) ;
			Schema schema = schemaFactory.newSchema( new StreamSource(XSDFileSchema) ) ;
			Validator validator = schema.newValidator() ;
			
			// valid ? null : exception
			validator.validate( new StreamSource( xml_file ) ) ;
			
			return true ;
		} 
		catch ( Exception e ) 
		{ 
			e.printStackTrace(); 
			return false ;
		}
	}
	
}