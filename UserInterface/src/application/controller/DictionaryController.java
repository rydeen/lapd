package application.controller;

import java.io.File;

import application.model.Main;
import dictionary_parser.Parser;
import dictionary_parser.XMLCategory;
import dictionary_parser.XMLFileValidator;
import dictionary_parser.XMLFileWriter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.SVGPath;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class DictionaryController {

	@FXML private Text text_filepath ;
	@FXML private TextField text_filename ;
	@FXML private Pane errorPane ;
	@FXML private Pane informPane ;
	@FXML private Pane errorPaneLi ;
	@FXML private Pane informPaneLi ;
	@FXML private Text text_error ;
	@FXML private Text text_inform ;
	@FXML private Text text_errorLi ;
	@FXML private Text text_informLi ;
	
	@FXML private Pane DictionaryPane ;
	@FXML private Pane DicListContent ;

	public File selectedFile ;
	public boolean fileselected = false ;
	private int spaceBetweenLi = 40 ;
	
	@FXML
	protected void chooseFile( ActionEvent event )
	{
		this.errorPane.setVisible(false);
		this.informPane.setVisible(false);
		
		FileChooser fileChooser = new FileChooser() ;
				
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Excel Files (.xls)", "*.xls"));

		this.selectedFile = fileChooser.showOpenDialog(null) ;
		
		if ( this.selectedFile != null )
		{
			this.text_filepath.setText( this.selectedFile.getName() );
			this.fileselected = true ;
		}
		else
		{
			this.text_filepath.setText( "Nenhum ficheiro selecionado." );
			this.fileselected = false ;
		}
		
	}
	
	@FXML
	protected void createDictionary()
	{
		if ( this.fileselected ) 
		{
			this.errorPane.setVisible(false);
			String name = this.text_filename.getText() ;

			if ( name.equals("") ) {
				name = this.selectedFile.getName() ;
				int x = name.lastIndexOf(".") ;
				name = name.substring(0, x) ;
			}
		
			if ( verifyName(name) ) 
			{
				if ( ! Main.dictionaryDB.exists(name) )
				{
					XMLCategory dictionary = Parser.ParseExcelDictionary( this.selectedFile.getAbsolutePath() , name ) ;
					
					if( dictionary != null ) {
						File xmlDictionary = XMLFileWriter.CreateXMLFile( dictionary , "db/"+name+XMLFileWriter.XMLextension ) ;
						boolean done = XMLFileValidator.ValidadeXMLFile( xmlDictionary ) ;
						
						Main.dictionaryDB.addNewFile( name, "db/"+name+".xml" ) ;
						Main.dictionaryDB.saveChanges() ;
						
						if ( xmlDictionary!=null && done )
						{
							this.text_inform.setText(Messages.add_success) ;
							this.informPane.setVisible(true) ;
							this.errorPane.setVisible(false) ;
							this.fileselected = false ;
							this.selectedFile = null ;
							this.text_filename.setText("");
							this.text_filepath.setText("Nenhum ficheiro selecionado.");
							listDictionaries() ;
						}
						else
						{
							this.text_error.setText(Messages.validation_error) ;
							this.errorPane.setVisible(true) ;
							this.informPane.setVisible(false) ;
						}
					}
					else {
						this.text_error.setText(Messages.invalid_file) ;
						this.errorPane.setVisible(true) ;
						this.informPane.setVisible(false) ;
					}
				}
				else {
					this.text_error.setText(Messages.duplicate_file) ;
					this.errorPane.setVisible(true) ;
					this.informPane.setVisible(false) ;
				}
			}
			else {
				this.text_error.setText(Messages.invalid_filename) ;
				this.errorPane.setVisible(true) ;
				this.informPane.setVisible(false) ;
			}
		}
		else {
			this.text_error.setText(Messages.no_file_selected);
			this.errorPane.setVisible(true);
			this.informPane.setVisible(false) ;
		}
		
	}
	
	@FXML
	protected void reset() 
	{
		this.errorPane.setVisible(false);
		this.informPane.setVisible(false);
		this.informPaneLi.setVisible(false);
		this.errorPaneLi.setVisible(false);
		this.fileselected = false ;
		this.selectedFile = null ;
		this.text_filename.setText("");
		this.text_filepath.setText("Nenhum ficheiro selecionado.");
	}
	
	private boolean verifyName( String name )
	{
		if( XMLFileValidator.XSDNamePattern.matcher(name).matches() )
			return true ;
		else
			return false ;
	}

	protected void listDictionaries()
	{	
		this.DicListContent.getChildren().clear() ;
		this.DicListContent.setPrefHeight(200.0);
		this.DictionaryPane.setPrefHeight(568.0);
		
		int i = 0 ;
		for ( String key : Main.dictionaryDB.getMapOfFiles().keySet() )
		{
			addToGUI ( key , i ) ;
			i++ ;
		}
	}
	
	protected void addToGUI(String filename, int index )
	{			    
		Pane p = new Pane() ;
		p.setLayoutX(15.0); p.setLayoutY(30.0 + this.spaceBetweenLi*index); p.setPrefHeight(30.0); p.setPrefWidth(352.0);
		p.getStyleClass().add("liDictionary") ;
		
		/* filename */
		Text t = new Text( filename ) ;
		t.setLayoutX(14.0); t.setLayoutY(21.0); t.setStrokeType(StrokeType.OUTSIDE) ; t.setStrokeWidth(0.0); t.setFont(new Font(14)) ;
		
		/* edit button */
		Button b_edit = new Button("     Editar") ;
		b_edit.setLayoutX(450.0); b_edit.setPrefHeight(29.0); b_edit.setPrefWidth(71.0); b_edit.setMnemonicParsing(false); b_edit.getStyleClass().add("btnEdit") ;
		
		/* edit icon */
		SVGPath icon_edit = new SVGPath() ;
		icon_edit.setLayoutX(433.0); icon_edit.setLayoutY(-16.0); icon_edit.setScaleX(0.25); icon_edit.setScaleY(0.25); icon_edit.setFill( Color.WHITE ) ;
		icon_edit.setContent("M54 0c5.523 0 10 4.477 10 10 0 2.251-0.744 4.329-2 6l-4 4-14-14 4-4c1.671-1.256 3.749-2 6-2zM4 46l-4 18 18-4 37-37-14-14-37 37zM44.724 22.724l-28 28-3.447-3.447 28-28 3.447 3.447z") ;
		
		/* remove button */
		Button b_del = new Button("    Remover") ;
		b_del.setLayoutX(523.0); b_del.setPrefHeight(29.0); b_del.setPrefWidth(87.0); b_del.setMnemonicParsing(false); b_del.getStyleClass().add("btnRemove") ; 
			
		/* remove icon */
		SVGPath icon_del1 = new SVGPath() ;
		icon_del1.setLayoutX(506.0); icon_del1.setLayoutY(2); icon_del1.setScaleX(0.25); icon_del1.setScaleY(0.25); icon_del1.setFill( Color.WHITE ) ;
		icon_del1.setContent("M53 8h-13v-5c0-1.65-1.35-3-3-3h-14c-1.65 0-3 1.35-3 3v5h-13c-1.65 0-3 1.35-3 3v5h52v-5c0-1.65-1.35-3-3-3zM36 8h-12v-3.95h12v3.95z");
		SVGPath icon_del2 = new SVGPath() ;
		icon_del2.setLayoutX(506.0); icon_del2.setLayoutY(-23.0); icon_del2.setScaleX(0.25); icon_del2.setScaleY(0.25); icon_del2.setFill( Color.WHITE ) ;
		icon_del2.setContent("M8 20v40c0 2.2 1.8 4 4 4h36c2.2 0 4-1.8 4-4v-40h-44zM20 56h-4v-28h4v28zM28 56h-4v-28h4v28zM36 56h-4v-28h4v28zM44 56h-4v-28h4v28z") ;
		
		/* show button */
		Button b_show = new Button("     Consultar") ;
		b_show.setLayoutX(354.0); b_show.setPrefHeight(29.0); b_show.setPrefWidth(94.0); b_show.setMnemonicParsing(false); b_show.getStyleClass().add("btnShow") ;
		
		/* show icon */
		SVGPath icon_show = new SVGPath() ;
		icon_show.setLayoutX(337.0); icon_show.setLayoutY(-16.0); icon_show.setScaleX(0.25); icon_show.setScaleY(0.25); icon_show.setFill( Color.WHITE ) ;
		icon_show.setContent("M32 12c-13.957 0-26.055 8.128-32 20 5.945 11.872 18.043 20 32 20s26.055-8.128 32-20c-5.945-11.872-18.043-20-32-20zM47.778 22.607c3.76 2.398 6.946 5.611 9.34 9.393-2.393 3.783-5.58 6.995-9.34 9.394-4.725 3.014-10.181 4.606-15.778 4.606s-11.054-1.593-15.778-4.606c-3.76-2.398-6.946-5.611-9.339-9.394 2.393-3.783 5.58-6.995 9.34-9.393 0.245-0.156 0.492-0.308 0.741-0.456-0.622 1.708-0.963 3.551-0.963 5.475 0 8.836 7.163 16 16 16s16-7.164 16-16c0-1.923-0.341-3.767-0.963-5.475 0.249 0.149 0.496 0.3 0.741 0.456v0zM32 26c0 3.314-2.686 6-6 6s-6-2.686-6-6 2.686-6 6-6 6 2.686 6 6z");
		
		b_edit.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	TextField tf = new TextField( filename ) ;
            	tf.setLayoutX(1.0); tf.setLayoutY(1.0) ; tf.getStyleClass().add("text_filename_edit") ; tf.setPrefWidth(351.0);
            	p.getChildren().remove( t ) ;
            	p.getChildren().add ( tf ) ;
            	p.getStyleClass().add("liDictionaryEdit") ;
            	
            	p.getChildren().remove( b_edit ) ;
            	p.getChildren().remove( icon_edit ) ;
            	p.getChildren().remove( b_del ) ;
            	p.getChildren().remove( icon_del1 ) ;
            	p.getChildren().remove( icon_del2 ) ;
            	p.getChildren().remove( b_show ) ;
            	p.getChildren().remove( icon_show ) ;
            	
            	
            	Button discard = new Button("Cancelar") ;
            	discard.setLayoutX(354.0); discard.setPrefHeight(29.0); discard.setPrefWidth(127.0); discard.setMnemonicParsing(false); discard.getStyleClass().add("btnDiscard") ;
            	
            	Button save = new Button("Guardar") ;
            	save.setLayoutX(483.0); save.setPrefHeight(29.0); save.setPrefWidth(127.0); save.setMnemonicParsing(false); save.getStyleClass().add("btnSave") ;
            	
            	discard.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {                    	
                    	informPaneLi.setVisible(false) ;
            			errorPaneLi.setVisible(false) ;
            			p.getChildren().remove( save ) ;
                    	p.getChildren().remove( discard ) ;
                    	p.getChildren().add( b_edit ) ;
                    	p.getChildren().add( icon_edit ) ;
                    	p.getChildren().add( b_del ) ;
                    	p.getChildren().add( icon_del1 ) ;
                    	p.getChildren().add( icon_del2 ) ;
                    	p.getChildren().add( b_show ) ;
                    	p.getChildren().add( icon_show ) ;
                    	p.getChildren().add( t ) ;
                    	p.getChildren().remove ( tf ) ;
                    	p.getStyleClass().remove("liDictionaryEdit") ;
                    	p.getStyleClass().remove("liDictionaryEditError") ;
                    }}) ;
            	
            	save.setOnAction(new EventHandler<ActionEvent>() {
                    @Override public void handle(ActionEvent e) {
                    	switch( Main.dictionaryDB.changeName( t.getText() , tf.getText() )  )
                		{
                    		case 0: 
                    			text_informLi.setText(Messages.changeName_Success) ;
                    			informPaneLi.setVisible(true) ;
                    			errorPaneLi.setVisible(false) ;
                    			Main.dictionaryDB.saveChanges();
                    			listDictionaries();
                    			break;
                    		case -1: 
                    			text_errorLi.setText(Messages.fileNotFound) ;
                    			informPaneLi.setVisible(false) ;
                    			errorPaneLi.setVisible(true) ;
                    			p.getStyleClass().add("liDictionaryEditError") ;
                    			tf.getStyleClass().add("text_filename_edit_error");
                    			break;
                    		case -2: 
                    			text_errorLi.setText(Messages.duplicate_file) ;
                    			informPaneLi.setVisible(false) ;
                    			errorPaneLi.setVisible(true) ;
                    			p.getStyleClass().add("liDictionaryEditError") ;
                    			tf.getStyleClass().add("text_filename_edit_error");
                    			break;
                    		case -3: 
                    			text_errorLi.setText(Messages.changesFail) ;
                    			informPaneLi.setVisible(false) ;
                    			errorPaneLi.setVisible(true) ;
                    			p.getStyleClass().add("liDictionaryEditError") ;
                    			tf.getStyleClass().add("text_filename_edit_error");
                    			break;
                		}
                    }}) ;
            	
            	p.getChildren().add( save ) ;
            	p.getChildren().add( discard ) ;
            }
        });
		b_del.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
            	DicListContent.getChildren().remove(p) ;
            	Main.dictionaryDB.deleteFile(filename);
            	Main.dictionaryDB.saveChanges();
            }
        });
		b_show.setOnAction( new EventHandler<ActionEvent>() {
			@Override public void handle( ActionEvent e){
				DictionaryPane.setPrefHeight(553.0);
				DictionaryPane.getChildren().clear();
				Main.guiViews.dictionaryShowController.setDictionaryName( filename );
				Main.guiViews.dictionaryShowController.openDictionary( filename );
				DictionaryPane.getChildren().add( Main.guiViews.dictionaryShow ) ;
			}
		});
		
		p.getChildren().add( t ) ;
		p.getChildren().add( b_edit ) ;
		p.getChildren().add( icon_edit ) ;
		p.getChildren().add( b_del ) ;
		p.getChildren().add( icon_del1 ) ;
		p.getChildren().add( icon_del2 ) ;
		p.getChildren().add( b_show ) ;
		p.getChildren().add( icon_show ) ;
				
		if ( index > 3 && p.getLayoutY()>(this.DicListContent.getPrefHeight()-this.spaceBetweenLi) )
		{
			this.DictionaryPane.setPrefHeight( this.DictionaryPane.getPrefHeight() + this.spaceBetweenLi );
			this.DicListContent.setPrefHeight( this.DicListContent.getPrefHeight() + this.spaceBetweenLi );
		}
		
		this.DicListContent.getChildren().add( p ) ;
	}

}