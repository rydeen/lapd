package application.controller;

import frequency_counter.Frequency;
import frequency_counter.WordsFrequencyCounter;
import frequency_counter.WordsCollection;

import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;

import SerializableDB.PreferencesDB;
import application.model.Main;
import frequency_counter.TranscriptionFile;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

public class AnalysisController {
	
	/* Error panes */
	@FXML Pane errorDictionary ;
	@FXML Pane errorFile ;
	@FXML Pane errorActors ;
	@FXML Pane errorBlockInputs ;
	private boolean activeDistinctActors = false ;
	private boolean activeBlocks = false ;
	/* ----------- */
	
	@FXML Text actorsBoxLabel;
	@FXML Text blockSizeLabel;
	@FXML Text blockStepLabel;
	@FXML Text dictionaryLabel;
	
	@FXML TextArea ResultsText ;
	
	@FXML TextField BlockSizeInput ;
	@FXML TextField BlockStepInput ;
	
	@FXML Text text_filepath ;
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox DicChoiceBox ;
	
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox ActorsChoiceBox ;
	String actorSelected = null ;
	
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox MethodChoiceBox ;
	String methodSelected = null ;
	
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox DistinctActorChoiceBox ;
	String actorDistinctValueSelected = null ;
	
	public File selectedFile ;
	public boolean fileselected = false ;
	
	public TranscriptionFile transcriptionFile ;
	public static String ActorsChoiceAll = "Incluir todos" ;
	
	
	@FXML
	protected void chooseFile( ActionEvent event )
	{
		FileChooser fileChooser = new FileChooser() ;
				
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Word Files (.docx)", "*.docx"));

		this.selectedFile = fileChooser.showOpenDialog(null) ;
		
		if ( this.selectedFile != null )
		{
			this.text_filepath.setText( this.selectedFile.getName() );
			this.fileselected = true ;
			
			this.transcriptionFile = new TranscriptionFile(this.selectedFile.getAbsolutePath()) ;
			setActors() ;
		}
		else
		{
			this.text_filepath.setText( "Nenhum ficheiro selecionado." ) ;
			this.fileselected = false ;
			this.transcriptionFile = null ;
		}
		
	}
	
	@SuppressWarnings("unchecked")
	@FXML
	protected void reset() 
	{
		this.transcriptionFile = null ;
		this.fileselected = false ;
		this.selectedFile = null ;
		this.text_filepath.setText("Nenhum ficheiro selecionado.");
		this.DicChoiceBox.getSelectionModel().select( Main.preferencesDB.getDictionaryKey() );
		this.MethodChoiceBox.getSelectionModel().select( Main.preferencesDB.getMethod() );
		this.DistinctActorChoiceBox.getSelectionModel().select( Main.preferencesDB.getDistinctActorsValue() );
		this.ActorsChoiceBox.getItems().remove(0, this.ActorsChoiceBox.getItems().size());
		
		if ( Main.preferencesDB.getBlockSize()>0 )
			this.BlockSizeInput.setText( Integer.toString(Main.preferencesDB.getBlockSize()) );
		else
			this.BlockSizeInput.setText("");
		
		if ( Main.preferencesDB.getBlockStep()>0 )
			this.BlockStepInput.setText( Integer.toString(Main.preferencesDB.getBlockStep()) );
		else
			this.BlockStepInput.setText("");
		
		blockInputsError(false,0) ;
		fileError(false) ;
		dictionaryError(false) ;
		actorsError(false) ;
		
		this.activeBlocks = false ;
		this.activeDistinctActors = false ;
	}
	
	@SuppressWarnings("unchecked")
	public void setActors()
	{
		this.ActorsChoiceBox.getItems().add( AnalysisController.ActorsChoiceAll ) ;
		
		for ( Entry<String,String> entry : this.transcriptionFile.getActors().entrySet() )
		{
			this.ActorsChoiceBox.getItems().add(entry.getValue() ) ;
		}
		
		this.ActorsChoiceBox.getSelectionModel().select(0) ;
	}
	
	@SuppressWarnings("unchecked")
	public void setMode()
	{	
		this.MethodChoiceBox.getItems().remove( PreferencesDB.methodFull ) ;
		this.MethodChoiceBox.getItems().remove( PreferencesDB.methodBlock ) ;
		this.MethodChoiceBox.getItems().add( PreferencesDB.methodFull ) ;
		this.MethodChoiceBox.getItems().add( PreferencesDB.methodBlock ) ;
		
		this.MethodChoiceBox.valueProperty().addListener( new ChangeListener<String>(){
			@SuppressWarnings("rawtypes")
			@Override public void changed( ObservableValue ov, String t , String t1 ){
				if ( t1.equals( PreferencesDB.methodFull ) )
				{
					activeBlocks = false ;
					blockSizeLabel.setVisible( false );
					blockStepLabel.setVisible( false );
					BlockSizeInput.setVisible( false );
					BlockStepInput.setVisible( false );
				}
				else if ( t1.equals( PreferencesDB.methodBlock ) )
				{
					activeBlocks = true ;
					blockSizeLabel.setVisible( true );
					blockStepLabel.setVisible( true );
					BlockSizeInput.setVisible( true );
					BlockStepInput.setVisible( true );
				}
			}
		});
		
		this.MethodChoiceBox.getSelectionModel().select( Main.preferencesDB.getMethod() );
		
		if ( Main.preferencesDB.getMethod().equals( PreferencesDB.methodBlock) )
		{
			this.BlockSizeInput.setText( Integer.toString( Main.preferencesDB.getBlockSize() ) );
			this.BlockStepInput.setText( Integer.toString( Main.preferencesDB.getBlockStep() ) );
		}
		else
		{
			this.BlockSizeInput.setText( "" );
			this.BlockStepInput.setText( "" );
		}
	}
	
	@SuppressWarnings("unchecked")
	public void setDictionaries()
	{
		this.DicChoiceBox.getItems().remove(0,  this.DicChoiceBox.getItems().size());

		for( String s : Main.dictionaryDB.getMapOfFiles().keySet() )
			this.DicChoiceBox.getItems().add(s) ;
		
		this.DicChoiceBox.getSelectionModel().select( Main.preferencesDB.getDictionaryKey() );
	}
	
	@SuppressWarnings("unchecked")
	public void setDistinctActorOptions()
	{
		this.DistinctActorChoiceBox.getItems().remove( PreferencesDB.distinctNo ) ;
		this.DistinctActorChoiceBox.getItems().remove( PreferencesDB.distinctYes ) ;
		this.DistinctActorChoiceBox.getItems().add( PreferencesDB.distinctNo ) ;
		this.DistinctActorChoiceBox.getItems().add( PreferencesDB.distinctYes ) ;
		
		this.DistinctActorChoiceBox.getSelectionModel().select( Main.preferencesDB.getDistinctActorsValue() );
		
		this.DistinctActorChoiceBox.valueProperty().addListener( new ChangeListener<String>(){
			@SuppressWarnings("rawtypes")
			@Override public void changed(ObservableValue ov, String t, String t1) {
				if ( t1!= null && t1.equals( PreferencesDB.distinctYes  ) )
				{
					actorsBoxLabel.setVisible( true ) ;
					ActorsChoiceBox.setVisible( true );
				}
				else if (t1!=null && t1.equals( PreferencesDB.distinctNo ) )
				{
					actorsBoxLabel.setVisible( false ) ;
					ActorsChoiceBox.setVisible( false );
				}
			} 
			
		});
	}
		
	public boolean verifyForm()
	{		
		boolean result = true ;
		
		/* ------ transcription file select ------ */
		if ( ! fileselected )
		{
			fileError(true) ;
			result = false ;
		}
		else
			fileError(false) ;
			
		/* ------ dictionary selected ------ */
		if ( this.DicChoiceBox.getSelectionModel().getSelectedItem() == null )
		{
			dictionaryError(true) ;
			result = false ;
		}
		else
			dictionaryError(false) ;
		
		/* ------ Actors ------ */
		// an error will never occur because there is a selected default value when distinctActors=true
		if ( this.activeDistinctActors )
		{
			if ( this.ActorsChoiceBox.getSelectionModel().getSelectedItem() == null )
			{
				actorsError(true) ;
				result = false ;
			}
		}
		
		/* ------ block inputs ------ */
		if ( this.activeBlocks )
		{
			if ( this.BlockSizeInput.getText() != null && this.BlockStepInput.getText() != null )
			{
				boolean sizeError = false ;
				boolean stepError = false ;
				int size = 0, step = 0;
				
				try{
					size = Integer.parseInt( this.BlockSizeInput.getText() ) ;
				}catch(NumberFormatException | NullPointerException e){
					sizeError = true ;
					result=false;
				}
				try{
					step = Integer.parseInt( this.BlockStepInput.getText() ) ;
				}catch(NumberFormatException | NullPointerException e){
					stepError = true ;
					result=false;
				}
				
				if ( sizeError && stepError )
					blockInputsError(true,0) ;
				else if ( sizeError )
					blockInputsError(true,1) ;
				else if ( stepError )
					blockInputsError(true,2) ;
				else
				{
					if ( size>0 && step>0 && size>step )
						blockInputsError(false,0);
					else
						blockInputsError(true,0) ;
				}
			}
			else
			{
				result = false ;
				blockInputsError(true,0) ;
			}
		}
		else
			blockInputsError(false,0) ;
		
		
		return result ;
	}
	
	@FXML
	public void start()
	{	
		if ( ! verifyForm() )
			return ;
		
		
		// if distinct actors
		if ( this.DistinctActorChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.distinctYes) )
		{
			WordsCollection wc_actors = new WordsCollection( this.DicChoiceBox.getSelectionModel().getSelectedItem().toString() , this.transcriptionFile.getActors() ) ;
			
			// if all
			if ( this.ActorsChoiceBox.getSelectionModel().getSelectedItem().toString().equals( AnalysisController.ActorsChoiceAll ) )
			{
				// if full text
				if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodFull) )
				{
					System.out.println("1");
					HashMap<String, HashMap<String,Frequency> > results1 = WordsFrequencyCounter.countFulltext(this.transcriptionFile,wc_actors,true) ;
					showResults1(results1);
				}
				// else if blocks
				else if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodBlock) )
				{
					// tamanho + step
					System.out.println("2");
					HashMap<String, Frequency> results2 =  WordsFrequencyCounter.countFulltextByActor(this.transcriptionFile,"T",wc_actors) ;
					showResults25(results2);
				}
			}
			// else if actor X
			else
			{
				// if full text
				if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodFull) )
				{
					for ( Entry<String,String> entry : this.transcriptionFile.getActors().entrySet() )
					{
						if( this.ActorsChoiceBox.getSelectionModel().getSelectedItem().toString().equals(entry.getValue()) )
						{
							System.out.println("3");
							HashMap<Integer, WordsCollection> results3 =
									WordsFrequencyCounter.countFulltextBlocks(this.transcriptionFile,wc_actors,true,Integer.parseInt(this.BlockSizeInput.getText()), Integer.parseInt(this.BlockStepInput.getText()));
							showResults346(results3);
						}
					}
				}
				// else if blocks
				else if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodBlock) )
				{
					// tamanho + step
					WordsCollection wc_noActors = new WordsCollection( this.DicChoiceBox.getSelectionModel().getSelectedItem().toString() ) ;

					for ( Entry<String,String> entry : this.transcriptionFile.getActors().entrySet() )
					{
						if( this.ActorsChoiceBox.getSelectionModel().getSelectedItem().toString().equals(entry.getValue()) )
						{
							System.out.println("4");
							HashMap<Integer, WordsCollection> results4 =
									WordsFrequencyCounter.countFulltextBlocksByActor(this.transcriptionFile,wc_noActors,"T",Integer.parseInt(this.BlockSizeInput.getText()), Integer.parseInt(this.BlockStepInput.getText()));
							showResults346(results4);
							break;
						}
					}
					
				}
			}
		}
		// else if not distinct actors
		else if ( this.DistinctActorChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.distinctNo) )
		{
			WordsCollection wc_noActors = new WordsCollection( this.DicChoiceBox.getSelectionModel().getSelectedItem().toString() ) ;
			
			// if full text
			if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodFull) )
			{
				System.out.println("5");
				WordsFrequencyCounter.countFulltext(this.transcriptionFile,wc_noActors,false) ;
				HashMap<String, Frequency> results5 =  wc_noActors.getResults(true) ;
				showResults25(results5);
			}
			// else if blocks
			else if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().toString().equals(PreferencesDB.methodBlock) )
			{
				System.out.println("6");
				HashMap<Integer, WordsCollection> results6 =
						WordsFrequencyCounter.countFulltextBlocks(this.transcriptionFile,wc_noActors,false,Integer.parseInt(this.BlockSizeInput.getText()), Integer.parseInt(this.BlockStepInput.getText()));
				showResults346(results6);
			}
		}
		
	}
	
	
	
	/* ------------ SHOW RESULTS ------------ */
	private void showResults1( HashMap<String, HashMap<String,Frequency> > results )
	{
		
	}
	private void showResults25( HashMap<String, Frequency> results )
	{
		
	}
	private void showResults346( HashMap<Integer, WordsCollection> results )
	{
		
	}


	/* ------------ ERROR MESSAGES ------------ */
	/**
	 * Display an error message when the 'size' or 'step' values are wrong
	 * input = 1 (error in 'size')
	 * input = 2 (error in 'step')
	 * input = 0 (error in both 'size' and 'step')
	 * @param display
	 * @param input
	 */
	public void blockInputsError( boolean display , int input )
	{
		this.errorBlockInputs.setVisible( display ) ;
		
		if ( display )
		{
			switch( input )
			{
				case 0: 
					this.BlockSizeInput.setStyle("-fx-border-color: #eb4546");
					this.BlockStepInput.setStyle("-fx-border-color: #eb4546");
					this.blockSizeLabel.setStyle("-fx-fill: #eb4546");
					this.blockStepLabel.setStyle("-fx-fill: #eb4546");
					break;
				case 1: 
					this.BlockSizeInput.setStyle("-fx-border-color: #eb4546");
					this.blockSizeLabel.setStyle("-fx-fill: #eb4546");
					this.BlockStepInput.setStyle("-fx-border-color: #045a80");
					this.blockStepLabel.setStyle("-fx-fill: #045a80");
					break;
				case 2: 
					this.BlockStepInput.setStyle("-fx-border-color: #eb4546");
					this.blockStepLabel.setStyle("-fx-fill: #eb4546");
					this.BlockSizeInput.setStyle("-fx-border-color: #045a80");
					this.blockSizeLabel.setStyle("-fx-fill: #045a80");
					break;
			}
		}
		else
		{
			this.BlockSizeInput.setStyle("-fx-border-color: #045a80");
			this.BlockStepInput.setStyle("-fx-border-color: #045a80");
			this.blockSizeLabel.setStyle("-fx-fill: #045a80");
			this.blockStepLabel.setStyle("-fx-fill: #045a80");
		}
	}
	public void fileError( boolean display )
	{
		this.errorFile.setVisible( display );
		if ( display )
		{
			this.text_filepath.setStyle("-fx-fill: #eb4546");
		}
		else
		{
			this.text_filepath.setStyle("-fx-fill: #045a80");
		}
		
	}
	public void dictionaryError( boolean display )
	{
		this.errorDictionary.setVisible( display );
		if ( display )
		{
			this.DicChoiceBox.setStyle("-fx-border-color: #eb4546");
		}
		else
		{
			this.DicChoiceBox.setStyle("-fx-border-color: #045a80");
		}
	}
	public void actorsError( boolean display )
	{
		this.errorActors.setVisible( display );
		if ( display )
		{
			this.ActorsChoiceBox.setStyle("-fx-border-color: #eb4546");
			this.actorsBoxLabel.setStyle("-fx-fill: #eb4546");
		}
		else
		{
			this.ActorsChoiceBox.setStyle("-fx-border-color: #045a80");
			this.actorsBoxLabel.setStyle("-fx-fill: #045a80");
		}
	}

}
