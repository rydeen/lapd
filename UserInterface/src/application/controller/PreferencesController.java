package application.controller;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import SerializableDB.PreferencesDB;
import application.model.Main;

public class PreferencesController {

	@FXML Pane informSuccessRestore ;
	@FXML Pane informSuccess ;
	@FXML Pane errorBlockInputs ;
	@FXML Pane errorDictionary ;
	
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox DicChoiceBox;
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox DistinctActorChoiceBox;
	@SuppressWarnings("rawtypes")
	@FXML ChoiceBox MethodChoiceBox ;
	
	@FXML TextField BlockSizeInput;
	@FXML TextField BlockStepInput;
	@FXML Text BlockSizeLabel ;
	@FXML Text BlockStepLabel ;
	
	@SuppressWarnings("unchecked")
	public void setMode()
	{	
		this.MethodChoiceBox.getItems().remove( PreferencesDB.methodFull ) ;
		this.MethodChoiceBox.getItems().remove( PreferencesDB.methodBlock ) ;
		this.MethodChoiceBox.getItems().add( PreferencesDB.methodFull ) ;
		this.MethodChoiceBox.getItems().add( PreferencesDB.methodBlock ) ;
		
		this.MethodChoiceBox.getSelectionModel().select( Main.preferencesDB.getMethod() );
	}
	@SuppressWarnings("unchecked")
	public void setDictionaries()
	{
		this.DicChoiceBox.getItems().remove(0,  this.DicChoiceBox.getItems().size());
		
		for( String s : Main.dictionaryDB.getMapOfFiles().keySet() )
			this.DicChoiceBox.getItems().add(s) ;
		
		this.DicChoiceBox.getSelectionModel().select( Main.preferencesDB.getDictionaryKey() );
		
		if ( ! Main.dictionaryDB.getMapOfFiles().containsKey( Main.preferencesDB.getDictionaryKey() ) )
		{
			Main.preferencesDB.restoreDictionaryKey() ;
			Main.preferencesDB.saveChanges();
		}
	}
	@SuppressWarnings("unchecked")
	public void setDistinctActorOptions()
	{
		this.DistinctActorChoiceBox.getItems().remove( PreferencesDB.distinctNo ) ;
		this.DistinctActorChoiceBox.getItems().remove( PreferencesDB.distinctYes ) ;
		this.DistinctActorChoiceBox.getItems().add( PreferencesDB.distinctNo ) ;
		this.DistinctActorChoiceBox.getItems().add( PreferencesDB.distinctYes ) ;
		
		this.DistinctActorChoiceBox.getSelectionModel().select( Main.preferencesDB.getDistinctActorsValue() );
	}
	public void setBlockInputs()
	{
		if ( Main.preferencesDB.getBlockSize() > 0 )
			this.BlockSizeInput.setText( Integer.toString(Main.preferencesDB.getBlockSize()) );
		else
			this.BlockSizeInput.setText("");
		
		if( Main.preferencesDB.getBlockStep() > 0 )
			this.BlockStepInput.setText( Integer.toString(Main.preferencesDB.getBlockStep()) );
		else
			this.BlockStepInput.setText("");
	}
	
	
	public boolean verify()
	{
		this.restoreSuccess(false);
		this.saveSuccess(false);
		
		boolean result = true ;
		
		/* block inputs */
		if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().equals( PreferencesDB.methodBlock) )
		{						
			boolean sizeError = false ;
			boolean stepError = false ;
			int size = 0, step = 0;
			
			try{
				size = Integer.parseInt( this.BlockSizeInput.getText() ) ;
			}catch(NumberFormatException | NullPointerException e){
				sizeError = true ;
				result=false;
			}
			try{
				step = Integer.parseInt( this.BlockStepInput.getText() ) ;
			}catch(NumberFormatException | NullPointerException e){
				stepError = true ;
				result=false;
			}
			
			if ( sizeError && stepError )
				this.blockInputsError(true, 0);
			else if ( sizeError )
				this.blockInputsError(true, 1);
			else if ( stepError )
				this.blockInputsError(true, 2);
			else
			{
				if ( size>0 && step>0 && size>step )
					this.blockInputsError(false, 0);
				else
					this.blockInputsError(true, 2);
			}
		}
		
		/* dictionaary key */
		if ( this.DicChoiceBox.getSelectionModel().getSelectedItem() == null )
		{
			this.dictionaryError(true);
			result = false ;
		}
		
		return result ;
	}
	
	
	public void reset() 
	{
		this.setMode();
		this.setDictionaries();
		this.setDistinctActorOptions();
		this.setBlockInputs();
		
		this.blockInputsError(false, 0);
		this.dictionaryError(false);
		this.restoreSuccess(false);
		this.saveSuccess(false);
	}
	public void restore()
	{
		Main.preferencesDB.restore() ;
		Main.preferencesDB.saveChanges() ;
		reset();
		this.restoreSuccess(true);
	}
	public void save()
	{
		if ( this.verify() )
		{
			Main.preferencesDB.setMethod( (String) this.MethodChoiceBox.getSelectionModel().getSelectedItem() ) ;
			Main.preferencesDB.setDistinctActors( (String) this.DistinctActorChoiceBox.getSelectionModel().getSelectedItem() ) ;
			Main.preferencesDB.setDictionaryKey( (String) this.DicChoiceBox.getSelectionModel().getSelectedItem() ) ;
			
			if ( this.MethodChoiceBox.getSelectionModel().getSelectedItem().equals( PreferencesDB.methodBlock) )
			{
				Main.preferencesDB.setBlockSize( Integer.parseInt(this.BlockSizeInput.getText()) ) ;
				Main.preferencesDB.setBlockStep( Integer.parseInt(this.BlockStepInput.getText()) ) ;
			}
			
			Main.preferencesDB.saveChanges() ;
			this.reset() ;
			this.saveSuccess(true);
		}
	}


	/* success messages */
	public void restoreSuccess( boolean display )
	{
		this.informSuccessRestore.setVisible( display );
	}
	public void saveSuccess( boolean display )
	{
		this.informSuccess.setVisible( display );
	}
	/* error messages */
	public void blockInputsError( boolean display , int input )
	{
		this.errorBlockInputs.setVisible( display ) ;
		
		if ( display )
		{
			switch( input )
			{
				case 0: 
					this.BlockSizeInput.setStyle("-fx-border-color: #eb4546");
					this.BlockStepInput.setStyle("-fx-border-color: #eb4546");
					this.BlockSizeLabel.setStyle("-fx-fill: #eb4546");
					this.BlockStepLabel.setStyle("-fx-fill: #eb4546");
					break;
				case 1: 
					this.BlockSizeInput.setStyle("-fx-border-color: #eb4546");
					this.BlockSizeLabel.setStyle("-fx-fill: #eb4546");
					this.BlockStepInput.setStyle("-fx-border-color: #045a80");
					this.BlockStepLabel.setStyle("-fx-fill: #045a80");
					break;
				case 2: 
					this.BlockStepInput.setStyle("-fx-border-color: #eb4546");
					this.BlockStepLabel.setStyle("-fx-fill: #eb4546");
					this.BlockSizeInput.setStyle("-fx-border-color: #045a80");
					this.BlockSizeLabel.setStyle("-fx-fill: #045a80");
					break;
			}
		}
		else
		{
			this.BlockSizeInput.setStyle("-fx-border-color: #045a80");
			this.BlockStepInput.setStyle("-fx-border-color: #045a80");
			this.BlockSizeLabel.setStyle("-fx-fill: #045a80");
			this.BlockStepLabel.setStyle("-fx-fill: #045a80");
		}
	}
	public void dictionaryError( boolean display )
	{
		this.errorDictionary.setVisible( display );
		if ( display )
		{
			this.DicChoiceBox.setStyle("-fx-border-color: #eb4546");
		}
		else
		{
			this.DicChoiceBox.setStyle("-fx-border-color: #045a80");
		}
	}

}
