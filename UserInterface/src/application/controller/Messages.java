package application.controller;

public class Messages 
{
	// ERROR MESSAGES
	
	protected static final String no_file_selected = "Selecione um ficheiro antes de adicionar!" ;
	protected static final String invalid_filename = "O nome é inválido. Utilize apenas letras, espaços e hífens!" ;
	protected static final String invalid_file = "O dicionário contém erros e não pode ser lido!" ;
	protected static final String validation_error = "Ocorreu um erro na validação do dicionário! Tente novamente." ;
	protected static final String duplicate_file = "Já existe um dicionário com o mesmo nome!" ;
	
	protected static final String fileNotFound = "O dicionário não existe! Por favor, contacte a equipa de suporte!" ;
	protected static final String changesFail = "Não foi possível efetuar as alterações." ;
		
	// INFORM MESSAGES
	
	protected static final String add_success = "O novo dicionário foi adicionado com sucesso." ;
	protected static final String changeName_Success = "O nome foi modificado com sucesso." ;
	
	
	
}
