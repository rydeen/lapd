package application.controller;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import application.model.Main;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextArea;
import javafx.scene.control.TitledPane;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class DictionaryShowController {

	@FXML Text dictionaryName ;
	@FXML Button returnToDictionaryView ;
	@FXML TextArea dictionaryContent ;
	
	@FXML ScrollPane AccordionScrollPane ;
	@FXML Accordion AccordionDictionary ;

	String dictionaryFilepath ;
	
	/**
	 * Set the name 'name' of the dictionary on GUI
	 * @param name
	 */
	public void setDictionaryName( String name )
	{
		this.dictionaryName.setText( name );
		this.dictionaryFilepath = Main.dictionaryDB.getMapOfFiles().get(name) ;	
	}
	
	/**
	 * Adds the content of the file "filename" to GUI
	 * @param filename
	 */
	public void openDictionary( String filename )
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null ;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		
		Document doc = null ;
		try {
			doc = builder.parse( new File( this.dictionaryFilepath ) ) ;
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		
		XPathExpression getFirstCategoryNodes = null ;
		try {
			getFirstCategoryNodes = xpath.compile("dicionario/categoria") ;
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		NodeList nodes = null ;
		try {
			nodes = (NodeList) getFirstCategoryNodes.evaluate( doc , XPathConstants.NODESET );
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		
		
		for ( int i=0 ; i<nodes.getLength() ; i++ )
		{
			// capitalize the first letter of the word
			String s =  nodes.item(i).getAttributes().item(0).getNodeValue().toLowerCase() ;
			s = s.substring(0, 1).toUpperCase() + s.substring(1) ;
						
			Accordion ac = new Accordion() ;
			TitledPane tp = new TitledPane( s , ac) ;
			
			
			this.AccordionDictionary.getPanes().add( tp ) ;
			
			NodeList nl = nodes.item(i).getChildNodes() ;
			for( int j=0 ; j<nl.getLength() ; j++ )
			{
				if( nl.item(j).getNodeName().equals("categoria") )
					recursiveCall( nl.item(j) , ac , 2 ) ;
				else if( nl.item(j).getNodeName().equals("palavras") )
					wordsCall( nl.item(j) , tp ) ;
			}

		}
		
		
		/* ------- Consume scroll horizontal event and hide horizontal scroll bar ------ */
		this.AccordionScrollPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		this.AccordionScrollPane.addEventFilter(ScrollEvent.SCROLL,new EventHandler<ScrollEvent>() {
	        @Override
	        public void handle(ScrollEvent event) {
	            if (event.getDeltaX() != 0) {
	                event.consume();
	            }
	        }
	    });
		
	}
	
	private void recursiveCall( Node node , Accordion accordion , int depth )
	{
		Accordion ac = new Accordion() ;
		
		// capitalize the first letter of the word
		String s = node.getAttributes().item(0).getNodeValue().toLowerCase() ;
		s = s.substring(0, 1).toUpperCase() + s.substring(1) ;
		
		TitledPane tp = new TitledPane( s , ac ) ;
		
		tp.setStyle("-fx-border-width: 3px " + depth*5 + "px");
		
		accordion.getPanes().add( tp ) ;
		
		NodeList nl = node.getChildNodes() ;
		for( int i=0 ; i<nl.getLength() ; i++ )
			if( nl.item(i).getNodeName().equals("categoria") )
				recursiveCall( node.getChildNodes().item(i) , ac , depth+1 ) ;
			else if( nl.item(i).getNodeName().equals("palavras") )
				wordsCall( nl.item(i) , tp ) ;
	}
	
	private void wordsCall( Node node , TitledPane pane )
	{
		Vector<Text> wordsTemp = new Vector<Text>() ;
		
		NodeList nl = node.getChildNodes() ; 
		GridPane grid = new GridPane();
		grid.setHgap( 20 );
		grid.setVgap( 5 );
		
		char lastLetter = 0 ;
		
		for( int i=0 ; i<nl.getLength() ; i++ )
		{
			Text t = new Text( nl.item(i).getChildNodes().item(0).getNodeValue().toLowerCase() ) ;
			
			char actualLetter = getFirstLetter(  t.getText() ) ;
			
			if ( actualLetter != lastLetter )
			{
				Text letter = new Text(Character.toString(actualLetter)) ;				
				letter.getStyleClass().add("word") ;
				wordsTemp.add( letter ) ;
				lastLetter = actualLetter ;
			}
			wordsTemp.add( t ) ;
		}
		
		int column = 0 , pos = 0 ;
		int columnSize = (int) Math.ceil( wordsTemp.size()/4.0f ) ;
				
		for( int i=0 ; i<wordsTemp.size() ; i++, pos++ )
		{
			if( pos > columnSize )
			{
				column++ ;
				pos=0;
			}
			
			grid.add( wordsTemp.get(i), column, pos );
			
		}		

		pane.setContent( grid );
	}
	
	private char getFirstLetter( String word )
	{
		return (Normalizer.normalize( word.trim().toUpperCase() , Normalizer.Form.NFD)).charAt(0) ;
	}
	
	/**
	 * Returns to previous page
	 * @throws IOException 
	 */
	public void returnToDictionaryView()
	{
		Main.guiViews.restoreDicitonaryView();
		try {
			Main.guiViews.rootController.openDictionary(null);
			this.AccordionDictionary.getPanes().clear();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}