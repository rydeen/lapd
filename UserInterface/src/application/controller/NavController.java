package application.controller;

import java.io.IOException;

import application.model.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

public class NavController {

	@FXML Pane ContentPane ;
	
	/*
	 * EXIT BUTTON
	 * */
	@FXML
	protected void exitApp( ActionEvent event )
	{
		Platform.exit() ;
	}
	
	/*
	 * DICTIONARY BUTTON
	 */
	@FXML
	protected void openDictionary( ActionEvent event ) throws IOException
	{
		this.ContentPane.getChildren().clear() ;
		this.ContentPane.getChildren().add( Main.guiViews.dictionary ) ;
		Main.guiViews.dictionaryController.reset(); 
		Main.guiViews.dictionaryController.listDictionaries();
	}
	
	
	/*
	 * ANALYSIS BUTTON
	 */	
	@FXML
	protected void openAnalysis( ActionEvent event )
	{
		this.ContentPane.getChildren().clear() ;
		this.ContentPane.getChildren().add( Main.guiViews.analysis ) ;
		Main.guiViews.analysisController.setMode();
		Main.guiViews.analysisController.setDictionaries();
		Main.guiViews.analysisController.setDistinctActorOptions();
	}
	
	
	/*
	 * PREFERENCES BUTTON
	 */	
	@FXML
	protected void openPreferences( ActionEvent event )
	{
		this.ContentPane.getChildren().clear();
		this.ContentPane.getChildren().add( Main.guiViews.preferences ) ;
		Main.guiViews.preferencesController.reset(); 
	}
	
	
	/*
	 * HELP BUTTON
	 */
	@FXML
	protected void openHelp( ActionEvent event )
	{
		this.ContentPane.getChildren().clear() ;	
		this.ContentPane.getChildren().add( Main.guiViews.help ) ;
	}
	
}
