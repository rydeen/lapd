package application.model;

import SerializableDB.DictionaryDB;
import SerializableDB.PreferencesDB;
import application.view.Views;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	
	
	public static DictionaryDB dictionaryDB ;
	public static PreferencesDB preferencesDB ;
	public static Views guiViews ;
	
	
	public static void main(String[] args)
	{
		Main.dictionaryDB = new DictionaryDB() ;
		Main.preferencesDB = new PreferencesDB() ;
		Main.guiViews = new Views() ;
		
		Main.dictionaryDB.loadFiles();
		Main.preferencesDB.loadFiles();
		Main.guiViews.load();
		
		launch(args) ;
	}
	
	@Override
	public void start(Stage primaryStage)
	{
		try {			
			Scene scene = new Scene( Main.guiViews.root ) ;
			scene.getStylesheets().clear() ;
			
			primaryStage.setTitle( "Luis Abreu | André Silva" );
			primaryStage.setScene ( scene ) ;
			primaryStage.show();
			primaryStage.setResizable(false);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
