package application.view;

import application.controller.AnalysisController;
import application.controller.DictionaryController;
import application.controller.DictionaryShowController;
import application.controller.NavController;
import application.controller.PreferencesController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class Views 
{
	private static final String mainView = "../view/MainView.fxml" ;
	private static final String dictionaryView = "../view/DictionaryView.fxml" ;
	private static final String dictionaryShowView = "../view/DictionaryShowView.fxml" ;
	private static final String analysisView = "../view/AnalysisView.fxml" ;
	private static final String preferencesView = "../view/PreferencesView.fxml" ;
	private static final String helpView = "../view/HelpView.fxml" ;
	
	
	public Parent root ;
	public Parent dictionary ;
	public Parent dictionaryShow ;
	public Parent preferences ;
	public Parent analysis ;
	public Parent help ;
	
	public NavController rootController ;
	public DictionaryController dictionaryController ;
	public DictionaryShowController dictionaryShowController ;
	public PreferencesController preferencesController ;
	public AnalysisController analysisController ;
	
	
	/**
	 * Loads all the application views and their controllers
	 */
	public void load()
	{
		try {
			FXMLLoader loader = new FXMLLoader() ;
			
			loader = new FXMLLoader(this.getClass().getResource(Views.mainView));
			this.root = (Parent) loader.load();
			this.rootController = loader.getController() ;
			
			loader = new FXMLLoader(this.getClass().getResource(Views.dictionaryView));
			this.dictionary = (Parent) loader.load();
			this.dictionaryController = loader.getController() ;
			
			loader = new FXMLLoader(this.getClass().getResource(Views.dictionaryShowView));
			this.dictionaryShow = (Parent) loader.load();
			this.dictionaryShowController = loader.getController() ;
			
			loader = new FXMLLoader(this.getClass().getResource(Views.analysisView));
			this.analysis = (Parent) loader.load();
			this.analysisController = loader.getController() ;
			
			loader = new FXMLLoader( this.getClass().getResource(Views.preferencesView)) ;
			this.preferences = (Parent) loader.load();
			this.preferencesController = loader.getController() ;
			
			loader = new FXMLLoader(this.getClass().getResource(Views.helpView ));
			this.help = (Parent) loader.load();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Reloads the 'DicitonaryView' and his controller
	 */
	public void restoreDicitonaryView()
	{
		try {
			FXMLLoader loader = new FXMLLoader() ;
			loader = new FXMLLoader(this.getClass().getResource(Views.dictionaryView));
			this.dictionary = (Parent) loader.load();
			this.dictionaryController = loader.getController() ;
		}
		catch( Exception e){
			e.printStackTrace();
		}
	}
}
