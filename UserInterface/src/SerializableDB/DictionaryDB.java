package SerializableDB;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;

import dictionary_parser.XMLFileWriter;

public class DictionaryDB implements Serializable {


	private static final long serialVersionUID = 1L;
	
	private final String serialFilename = "db/dicdb" ;
	private HashMap<String,String> filesMap;
	

	public DictionaryDB()
	{
		this.filesMap = new HashMap<String,String>() ;
	}
	
	
	
	/**
	 * Changes the name of a dictionary file 'filename' to 'newName'.
	 * Return 0 uppon success.
	 * Returns -1 if the changes failed
	 * Returns -2 if the name isn't available
	 * Returns -3 if the file don't exist in the db
	 * @param filename
	 * @param newName
	 * @param index
	 * @return
	 */
	public int changeName( String filename , String newName )
	{
		if ( this.filesMap.get( filename ) != null )
		{
			if ( this.filesMap.get( newName ) == null )
			{
				if ( XMLFileWriter.ChangeFilename( "db/"+filename , "db/"+newName ) != null )
				{
					this.filesMap.put( newName , "db/"+newName+".xml" ) ;
					this.filesMap.remove( filename ) ;
					
					return 0;
				}
				else
					return -1;
			}
			else
				return -2;
		}
		else
			return -3 ;
	}
	
	/**
	 * Returns a vector with the names of the files stored
	 * */
	public HashMap<String,String> getMapOfFiles() 
	{
		return this.filesMap ;	
	}
	
	/**
	 * Adds a new file to the records with the name 'filename'
	 * @param filename
	 */
	public void addNewFile ( String filename , String filepath ) 
	{
		this.filesMap.put(filename, filepath) ;
	}
	
	/**
	 * Removes the file in the positon 'index'
	 * @param index
	 */
	public void deleteFile ( String filenameKey )
	{
		System.out.println(this.filesMap.get( filenameKey ));
		(new File ( this.filesMap.get( filenameKey ) )).delete() ;
		this.filesMap.remove( filenameKey ) ;
	}
	
	
	/**
	 * Returns true if the file with the name 'filename' already exists, otherwise returns false
	 * @param filename
	 * @return
	 */
	public boolean exists( String filename )
	{
		return this.filesMap.containsKey( filename ) ;
	}
	
	
	/**
	 * Saves the state of the db
	 * */
	public void saveChanges()
	{
		try {
		    FileOutputStream fos = new FileOutputStream( this.serialFilename );
		    ObjectOutputStream out = new ObjectOutputStream(fos);
		    out.writeObject( this.filesMap );
		    out.flush();
		    out.close();
		}
		catch (Exception e) {
		    System.out.println(e); 
		}
	}
	
	
	
	/**
	 * Loads the state of the db
	 * */
	@SuppressWarnings("unchecked")
	public void loadFiles()
	{
		try {
			FileInputStream fis = new FileInputStream( this.serialFilename );
			ObjectInputStream in = new ObjectInputStream(fis);
			this.filesMap = (HashMap<String,String>)in.readObject();
			in.close();
		} catch (FileNotFoundException fnfe ) {
			this.filesMap = new HashMap<String,String>() ;
		} catch ( Exception e ){
			System.out.println("other");
		}
	}
	
}