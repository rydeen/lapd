package SerializableDB;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import application.model.Main;

public class PreferencesDB implements Serializable {

	public static String methodFull = "Anlisar documento por inteiro" ;
	public static String methodBlock = "Analisar documento por blocos" ;
	public static String distinctYes = "Sim" ;
	public static String distinctNo = "Não" ;

	private static final long serialVersionUID = 1L;
	
	private String serialFileName = "db/prefdb" ;

	private String distinctActors ;
	private String method ;
	private int blockSize ;
	private int blockStep ;
	private String dictionaryKey ;
	
	
	public PreferencesDB()
	{
		this.distinctActors = PreferencesDB.distinctNo  ;
		this.method = PreferencesDB.methodFull ;
		this.dictionaryKey = null ;
		this.blockSize = -1 ;
		this.blockStep = -1 ;
	}
	
	
	/**
	 * Changes the value of 'distinctActors'. Returns 'true' if succeeded.
	 * @param value
	 * @return
	 */
	public boolean setDistinctActors( String value )
	{
		if ( value.equals(PreferencesDB.distinctNo) || value.equals(PreferencesDB.distinctYes) )
		{
			this.distinctActors = value ;
			return true ;
		}
		else
			return false;
	}
	/**
	 * Changes the value of 'method'. Returns 'true' if succeeded.
	 * @param value
	 * @return
	 */
	public boolean setMethod( String value )
	{
		if ( value.equals(PreferencesDB.methodBlock) || value.equals(PreferencesDB.methodFull) )
		{
			this.method = value ;
			return true ;
		}
		else
			return false;
	}
	/**
	 * Changes the value of 'dictionaryKey'. Returns 'true' if succeeded.
	 * @param key
	 * @return
	 */
	public boolean setDictionaryKey( String key )
	{
		if ( Main.dictionaryDB.getMapOfFiles().containsKey( key ) )
		{
			this.dictionaryKey = key ;
			return true ;
		}
		else
			return false ;
	}
	/**
	 * Changes the value of 'blockSize'. Return true if succeeded (value > 0).
	 * @param value
	 * @return
	 */
	public boolean setBlockSize( int value )
	{
		if ( value > 0 )
		{
			this.blockSize = value ;
			return true ;
		}
		else
			return false ;
	}
	/**
	 * Changes the value of 'blockStep'. Return true if succeeded (value > 0 && value<'blockSize').
	 * @param value
	 * @return
	 */
	public boolean setBlockStep( int value )
	{
		if ( value > 0 && value < this.blockSize )
		{
			this.blockStep = value ;
			return true ;
		}
		else
			return false ;
	}

	
	/**
	 * Returns the value of 'distinctActors'
	 * @return
	 */
	public String getDistinctActorsValue()
	{
		return this.distinctActors ;
	}
	/**
	 * Returns the value of 'method'
	 * @return
	 */
	public String getMethod()
	{
		return this.method ;
	}
	/**
	 * Returns the 'dictionaryKey'
	 * @return
	 */
	public String getDictionaryKey()
	{
		return this.dictionaryKey ;
	}
	/**
	 * Returns the 'blockSize' if 'method'=PreferencesDB.methodBlock. Otherwise, returns -1.
	 * @return
	 */
	public int getBlockSize()
	{
		if ( this.method.equals(PreferencesDB.methodBlock) )
			return this.blockSize ;
		else
			return -1;
	}
	/**
	 * Returns the 'blockStep' if 'method'=Preferences.methodBlock. Otherwise, returns -1.
	 * @return
	 */
	public int getBlockStep()
	{
		if ( this.method.equals(PreferencesDB.methodBlock) )
			return this.blockStep ;
		else
			return -1;
	}

	
	/**
	 * Saves preferences to "DB"
	 * */
	public void saveChanges()
	{
		try {
		    FileOutputStream fos = new FileOutputStream( this.serialFileName );
		    ObjectOutputStream out = new ObjectOutputStream(fos);
		    out.writeObject( this.distinctActors );
		    out.writeObject( this.method );
		    out.writeObject( this.dictionaryKey );
		    out.writeObject( this.blockSize );
		    out.writeObject( this.blockStep );
		    out.flush();
		    out.close();
		}
		catch (Exception e) {
		    System.out.println(e); 
		}
	}
	/**
	 * Loads preferences from "DB"
	 * */
	public void loadFiles()
	{
		try {
			FileInputStream fis = new FileInputStream( this.serialFileName );
			ObjectInputStream in = new ObjectInputStream(fis);
			this.distinctActors = (String) in.readObject() ;
			this.method = (String) in.readObject() ;
			this.dictionaryKey = (String) in.readObject() ;
			this.blockSize = (int) in.readObject() ;
			this.blockStep = (int) in.readObject() ;
			in.close();
		} catch (FileNotFoundException fnfe ) {
			this.distinctActors = PreferencesDB.distinctNo  ;
			this.method = PreferencesDB.methodFull ;
			this.dictionaryKey = null ;
			this.blockSize = -1 ;
			this.blockStep = -1 ;
		} catch ( Exception e ){
			System.out.println("other");
		}
	}
	/**
	 * Restores the default values for all preferences
	 */
	public void restore()
	{
		this.distinctActors = PreferencesDB.distinctNo  ;
		this.method = PreferencesDB.methodFull ;
		this.dictionaryKey = null ;
		this.blockSize = -1 ;
		this.blockStep = -1 ;
	}
	/**
	 * Restores the value of the dictionaryKey to null
	 */
	public void restoreDictionaryKey()
	{
		this.dictionaryKey = null ;
	}

}
