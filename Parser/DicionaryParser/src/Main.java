import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;


public class Main {

	public static void main( String [] args ) 
	{	
		// parse excel file
		String dictionaryName = "dicionario" ;
		String excelDictionaryFileName = "BaseLexicalPortuguesa.xls" ;
		XMLCategory dictionary = Parser.ParseExcelDictionary( excelDictionaryFileName , dictionaryName ) ;
		
		// writing content to xml file
		String XMLFileName = "dicionario.xml" ;
		File xmlDictionary = XMLFileWriter.CreateXMLFile( dictionary , XMLFileName ) ;
		
		// validate xml file with xsd
		XMLFileValidator.ValidadeXMLFile( xmlDictionary ) ;
		
		try {
			XMLFileWriter.ChangeFilename(dictionaryName, "merda");
		} catch (XPathExpressionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
