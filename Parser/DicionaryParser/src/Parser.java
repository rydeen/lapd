import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Parser {
	
	public static XMLCategory ParseExcelDictionary( String filepath , String dictionaryName )
	{
		try
		{
			FileInputStream file = new FileInputStream( new File (filepath) ) ;
			
			HSSFWorkbook workbook = new HSSFWorkbook(file) ;
			HSSFSheet sheet = workbook.getSheetAt(0) ;
			
			boolean readWords ;
			
			int numberOfColumns = sheet.getRow(0).getPhysicalNumberOfCells() ;
			
			XMLCategory dictionary = new XMLCategory(dictionaryName) ;
			XMLCategory temp = dictionary ;
			
			for ( int i=0 ; i<numberOfColumns ; i++ )
			{
				readWords = false ;
				Iterator<Row> row_iter = sheet.iterator() ;
				
				temp = temp.getRoot() ;
				
				while( row_iter.hasNext() )
				{
					Row row = row_iter.next() ;
					Cell cell = row.getCell(i) ;
					
					if( cell == null )
					{
						if( readWords )
							break ;
						else
							continue ;
					}
					else
					{
						if( cell.getCellType()==Cell.CELL_TYPE_BLANK && readWords )
						{
							
							break ;
						}
						
						switch( cell.getCellType() ) 
						{
							case Cell.CELL_TYPE_FORMULA :
								readWords = true ;
								break;
							case Cell.CELL_TYPE_STRING :
								if(readWords)
									temp.addWord(cell.getStringCellValue());
								else
									temp = temp.addCategory(cell.getStringCellValue());
								break;
						}
					}
				}
				
			}
			
			workbook.close();
			file.close();
			
			// dictionary.print() ;
			
			return dictionary ;
			
		} 
		catch ( Exception e )
		{
			// e.printStackTrace();
			return null ;
		}
	}
	
}
